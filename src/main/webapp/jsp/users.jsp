<%--
  Created by IntelliJ IDEA.
  User: Z30
  Date: 24.04.2019
  Time: 9:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Teams</title>
    <style type="text/css">

        table {
            font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
            font-size: 14px;
            border-radius: 10px;
            border-spacing: 0;
            text-align: center;
        }

        th {
            background: #BCEBDD;
            color: white;
            text-shadow: 0 1px 1px #2D2020;
            padding: 10px 20px;
        }

        th, td {
            border-style: solid;
            border-width: 0 1px 1px 0;
            border-color: white;
        }

        th:first-child, td:first-child {
            text-align: left;
        }

        th:first-child {
            border-top-left-radius: 10px;
        }

        th:last-child {
            border-top-right-radius: 10px;
            border-right: none;
        }

        td {
            padding: 10px 20px;
            background: #F8E391;
        }

        tr:last-child td:first-child {
            border-radius: 0 0 0 10px;
        }

        tr:last-child td:last-child {
            border-radius: 0 0 10px 0;
        }

        tr td:last-child {
            border-right: none;
        }

        .c {
            border: 3px solid #333; /* Рамка */
            display: inline-block;
            padding: 5px 15px; /* Поля */
            text-decoration: none; /* Убираем подчёркивание */
            background: linear-gradient(to bottom, #fcfff4, #e9e9ce);
            color: black; /* Цвет текста */
        }

        .c:hover {
            box-shadow: 0 0 5px rgba(0, 0, 0, 0.3); /* Тень */
            background: linear-gradient(to bottom, #fcfff4, #e9e9ce); /* Градиент */
            color: #a00;
        }

    </style>
</head>
<body background="https://www.pixelstalk.net/wp-content/uploads/images1/Champions-backgrounds-HD.jpg">

<h1 style="color: white; text-align: center">List all teams in our cup!</h1>
<h3 class="c"><a href="/Game_war/">Back to start menu!</a></h3>
<div>
    <div>
        <c:forEach items="${team}" var="temp">
            <h1 style="color: white">${temp.teamName}</h1>
            <table>
                <tr>
                    <th>Number</th>
                    <th>Name</th>
                    <th>Strength</th>
                </tr>
                <c:forEach items="${temp.listAllPlayers}" var="player">
                    <tr>
                        <td>${player.id}</td>
                        <td>${player.name}</td>
                        <td>${player.strength}</td>
                    </tr>
                </c:forEach>
            </table>
        </c:forEach>
    </div>
</div>
</body>
</html>
