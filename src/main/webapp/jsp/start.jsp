<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>Start Program</title>
    </head>
    <style>
        h1 {
            color: white;
        }
        .c {
            border: 3px solid #333; /* Рамка */
            display: inline-block;
            padding: 5px 15px; /* Поля */
            text-decoration: none; /* Убираем подчёркивание */
            background: linear-gradient(to bottom, #fcfff4, #e9e9ce);
            color: black; /* Цвет текста */
        }
        .c:hover {
            box-shadow: 0 0 5px rgba(0,0,0,0.3); /* Тень */
            background: linear-gradient(to bottom, #fcfff4, #e9e9ce); /* Градиент */
            color: #a00;
        }
    </style>
    <body background="https://wallpapercave.com/wp/wp4275523.jpg">
        <h1 class="c"><a href="users">Show all teams</a></h1>
        <br>
        <h1 class="c"><a href="cup">Make cup</a></h1>
        <br>
        <h1 class="c"><a href="generate">Generate new teams</a></h1>
        <br>
        <h1 class="c"><a href="score">Show history games bet</a></h1>
        <br>
        <h1 class="c"><a href="champions">Show champions</a></h1>
    </body>
</html>
