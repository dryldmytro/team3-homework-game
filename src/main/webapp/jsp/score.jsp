<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>History games</title>
    <style>
        table {
            font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
            font-size: 14px;
            border-radius: 10px;
            border-spacing: 0;
            width: 500px;
            margin: auto;
        }

        th {
            background: #BCEBDD;
            color: white;
            text-shadow: 0 1px 1px #2D2020;
            padding: 10px 20px;
        }

        th, td {
            border-style: solid;
            border-width: 0 1px 1px 0;
            border-color: white;
        }

        th:first-child, td:first-child {
            text-align: center;
        }

        th:first-child {
            border-top-left-radius: 10px;
        }

        th:last-child {
            border-top-right-radius: 10px;
            border-right: none;
        }

        td {
            padding: 10px 20px;
            text-align: center;
            background: #F8E391;
        }

        tr:last-child td:first-child {
            border-radius: 0 0 0 10px;
        }

        tr:last-child td:last-child {
            border-radius: 0 0 10px 0;
        }

        tr td:last-child {
            border-right: none;
        }

        .c {
            border: 3px solid #333; /* Рамка */
            display: inline-block;
            padding: 5px 15px; /* Поля */
            text-decoration: none; /* Убираем подчёркивание */
            background: linear-gradient(to bottom, #fcfff4, #e9e9ce);
            color: black; /* Цвет текста */
        }

        .c:hover {
            box-shadow: 0 0 5px rgba(0, 0, 0, 0.3); /* Тень */
            background: linear-gradient(to bottom, #fcfff4, #e9e9ce); /* Градиент */
            color: #a00;
        }
    </style>
</head>
<body background="https://www.pixelstalk.net/wp-content/uploads/images1/Free-uefa-champions-league-wallpapers.jpg">

<div style="text-align: center">
    <p class="c"><a href="/Game_war/">Back to start menu!</a></p>
    <h2 style="color: white">Your history</h2>
    <c:choose>
        <c:when test="${scoreList.size() == 0}">
            <h1 style="color: white">You did not do any attempt</h1>
        </c:when>
        <c:otherwise>
            <table>
                <tr>
                    <th>Name</th>
                    <th>Score</th>
                </tr>
                <c:forEach items="${scoreList}" var="temp">
                    <tr>
                        <td>${temp.name}</td>
                        <td style="color: #aa0000">${temp.score}</td>
                    </tr>
                </c:forEach>
            </table>
            <h1 style="color: white">The best attempt:</h1>
            <h1 style="color: red">${bestAttempt.name} score: ${bestAttempt.score}</h1>
        </c:otherwise>
    </c:choose>
</div>

</body>
</html>
