<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Cup</title>
    <style type="text/css">

        table {
            font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
            font-size: 14px;
            border-radius: 10px;
            border-spacing: 0;
            width: 500px;
            margin: auto;
        }

        th {
            background: #BCEBDD;
            color: white;
            text-shadow: 0 1px 1px #2D2020;
            padding: 10px 20px;
        }

        th, td {
            border-style: solid;
            border-width: 0 1px 1px 0;
            border-color: white;
        }

        th:first-child, td:first-child {
            text-align: center;
        }

        th:first-child {
            border-top-left-radius: 10px;
        }

        th:last-child {
            border-top-right-radius: 10px;
            border-right: none;
        }

        td {
            padding: 10px 20px;
            background: #F8E391;
        }

        tr:last-child td:first-child {
            border-radius: 0 0 0 10px;
        }

        tr:last-child td:last-child {
            border-radius: 0 0 10px 0;
        }

        tr td:last-child {
            border-right: none;
        }

        input:invalid {
            border: 2px dashed red;
        }

        input:valid {
            border: 2px solid black;
        }

        .c {
            border: 3px solid #333; /* Рамка */
            display: inline-block;
            padding: 5px 15px; /* Поля */
            text-decoration: none; /* Убираем подчёркивание */
            background: linear-gradient(to bottom, #fcfff4, #e9e9ce);
            color: black; /* Цвет текста */
        }

        .c:hover {
            box-shadow: 0 0 5px rgba(0, 0, 0, 0.3); /* Тень */
            background: linear-gradient(to bottom, #fcfff4, #e9e9ce); /* Градиент */
            color: #a00;
        }

    </style>
</head>
<body background="https://www.pixelstalk.net/wp-content/uploads/images1/Champions-backgrounds-HD.jpg">

<p class="c"><a href="/Game_war/">Back to start menu!</a></p>

<div style="text-align: center;">
    <h2 style="color: white">Teams</h2>
    <table style="text-align: center">
        <tr>
            <th>ID</th>
            <th>Name</th>
        </tr>
        <c:forEach items="${team}" var="temp">
            <tr>
                <td>${temp.id}</td>
                <td>${temp.teamName}</td>
            </tr>
        </c:forEach>
    </table>
    <c:choose>
        <c:when test="${finish == 1}">
            <h2>You have finish cup!</h2>
            <h2 style="color: red">Winner is ${team.get(0).teamName}</h2>
            <h2>Count of your money : ${maxValueForBet}</h2>
            <c:choose>
                <c:when test="${maxValueForBet > 1000}">
                    <h2 style="color: red">You have won : ${maxValueForBet-1000}</h2>
                </c:when>
                <c:when test="${maxValueForBet == 1000}">
                    <h2 style="color: red">Your money the same : ${maxValueForBet}</h2>
                </c:when>
                <c:otherwise>
                    <h2 style="color: red">You have lost : ${1000-maxValueForBet}</h2>
                </c:otherwise>
            </c:choose>
        </c:when>
        <c:otherwise>
            <c:if test="${maxValueForBet != 0}">
                <form method="post" action="ppp">
                    <p><input type="number" name="id" size="500" required min="0" max="${maxValueTeam}"
                              placeholder="Team:">
                        <input type="number" name="count" size="500" required min="0" max="${maxValueForBet}"
                               placeholder="Bet:"></p>
                    <p><input type="submit" value="Play">
                        <input type="reset" value="Clear"></p>
                </form>
            </c:if>
            <c:if test="${maxValueForBet == 0}">
                <form method="post" action="ppp">
                    <p><input type="submit" value="Play">
                        <input type="reset" value="Clear"></p>
                </form>
            </c:if>
        </c:otherwise>
    </c:choose>
</div>

</body>
</html>

