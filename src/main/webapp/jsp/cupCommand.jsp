<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Results</title>
    <style type="text/css">

        table {
            font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
            font-size: 14px;
            border-radius: 10px;
            border-spacing: 0;
            width: 400px;
            margin: auto;
        }

        th {
            background: #BCEBDD;
            color: white;
            text-shadow: 0 1px 1px #2D2020;
            padding: 10px 20px;
        }

        th, td {
            border-style: solid;
            border-width: 0 1px 1px 0;
            border-color: white;
        }

        th:first-child, td:first-child {
            text-align: left;
        }

        th:first-child {
            border-top-left-radius: 10px;
        }

        th:last-child {
            border-top-right-radius: 10px;
            border-right: none;
        }

        td {
            padding: 10px 20px;
            background: #F8E391;
        }

        tr:last-child td:first-child {
            border-radius: 0 0 0 10px;
        }

        tr:last-child td:last-child {
            border-radius: 0 0 10px 0;
        }

        tr td:last-child {
            border-right: none;
        }

        #center {
            margin-left: auto;
            margin-right: auto;
            width: 300px;
        }

        .c {
            border: 3px solid #333; /* Рамка */
            display: inline-block;
            padding: 5px 15px; /* Поля */
            text-decoration: none; /* Убираем подчёркивание */
            background: linear-gradient(to bottom, #fcfff4, #e9e9ce);
            color: black; /* Цвет текста */
        }

        .c:hover {
            box-shadow: 0 0 5px rgba(0, 0, 0, 0.3); /* Тень */
            background: linear-gradient(to bottom, #fcfff4, #e9e9ce); /* Градиент */
            color: #a00;
        }

    </style>
</head>
<body background="https://www.pixelstalk.net/wp-content/uploads/images1/Champions-backgrounds-HD.jpg">

<h1 style="text-align: center; color: white">Result of round!</h1>
<br>
<div style="text-align: center;">
    <table>
        <tr>
            <th style="color: red">Results</th>
        </tr>
        <c:forEach items="${str}" var="temp">
            <tr>
                <td>${temp}</td>
            </tr>
        </c:forEach>
    </table>

    <h2 style="color: white">Count money on gamer count: ${count}</h2>

    <br>

    <h2><a class="c" href="/Game_war/cup">Continue game!</a></h2>
</div>

</body>
</html>

