package com.thirdteam;

import com.thirdteam.Controller.ControllerImpl;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        ControllerImpl controller = new ControllerImpl();
        controller.createListTeams();
        controller.play();

    }
}
