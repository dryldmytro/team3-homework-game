package com.thirdteam.model.match;

import com.thirdteam.model.team.TeamImpl;

import java.util.List;

public interface Match {
    void play(TeamImpl firstTeam, TeamImpl secondTeam);
}
