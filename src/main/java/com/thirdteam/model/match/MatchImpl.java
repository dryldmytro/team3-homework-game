package com.thirdteam.model.match;


import com.thirdteam.model.team.TeamImpl;
import java.util.Random;

public class MatchImpl implements Match {
    public void play(TeamImpl firstTeam, TeamImpl secondTeam) {
        Random random = new Random();
        int winner = 1 + random.nextInt(6);
        int looser = random.nextInt(winner);
        int firstTeamStregth = firstTeam.getStrengthPlayersWhichPlay();
        int secondTeamStrength = secondTeam.getStrengthPlayersWhichPlay();
        double percent = firstTeamStregth + secondTeamStrength;
        percent = firstTeamStregth / percent * 100;
        double randomCount = Math.random() * 100;
        if (randomCount <= percent) {
            secondTeam.setAlive(false);
            System.out.println("Match " + firstTeam.getTeamName() + " - " + secondTeam.getTeamName()
                    + " ended with= " + winner + " : " + looser);
        } else {
            firstTeam.setAlive(false);
            System.out.println("Match " + firstTeam.getTeamName() + " - " + secondTeam.getTeamName()
                    + " ended with= " + looser + " : " + winner);
        }
    }


}
