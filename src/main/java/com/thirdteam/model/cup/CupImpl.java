package com.thirdteam.model.cup;

import com.thirdteam.model.match.Match;
import com.thirdteam.model.match.MatchImpl;
import com.thirdteam.model.player.PlayerImpl;
import com.thirdteam.model.team.TeamImpl;
import com.thirdteam.model.team.TeamNameComparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class CupImpl implements Cup {
    private final int QUANTITY_TEAMS = 16;
    private List<TeamImpl> listTeams = new ArrayList<TeamImpl>();
    private TeamNameComparator nameComparator = new TeamNameComparator();
    private Match match = new MatchImpl();
    private TeamImpl myTeam;

    public CupImpl() {
    }

    public void printTeamList() {
        for (TeamImpl team : listTeams) {
            System.out.println("id - " + team.getId() + "   " + "name - " + team.getTeamName().toUpperCase());
        }
    }

    public List<TeamImpl> getListTeams() {
        return listTeams;
    }

    public TeamImpl getMyTeam() {
        return myTeam;
    }

    public void setMyTeam(TeamImpl myTeam) {
        this.myTeam = myTeam;
    }

    public void printTeamListSortByName() {
        listTeams.sort(nameComparator);
        printTeamList();

    }

    public void fillListTeams() {
        for (int i = 0; i < QUANTITY_TEAMS; i++) {
            TeamImpl team = new TeamImpl(i);
            team.setListAllPlayers(getRandomTeam());
            this.listTeams.add(team);
        }
        Collections.shuffle(listTeams);
    }

    public List<PlayerImpl> getRandomTeam() {
        List<PlayerImpl> listRandomPlayers = new ArrayList<PlayerImpl>();
        for (int i = 0; i < TeamImpl.getQuantityPlayers(); i++) {
            listRandomPlayers.add(new PlayerImpl(i));
        }
        return listRandomPlayers;
    }

    public TeamImpl getTeamById(int id) {
        for (TeamImpl team : listTeams) {
            if (team.getId() == id) {
                return team;
            }
        }
        return null;
    }

    public void tournamentGrid() {
        for (int i = 0; i < listTeams.size(); i += 2) {
            if (listTeams.size() == 1) {
                System.out.println("End tournament");
                System.exit(0);
                break;
            }
            match.play(listTeams.get(i), listTeams.get(i + 1));
            changeStrength();
        }
        Collections.shuffle(listTeams);
        Iterator<TeamImpl> iterator = listTeams.listIterator();
        while (iterator.hasNext()) {
            TeamImpl team = iterator.next();
            if (!team.isAlive()) {
                iterator.remove();
            }
        }
    }

    public boolean checkIsWin() {
        if (listTeams.contains(myTeam)) {
            return true;
        }
        return false;
    }
    public void changeStrength(){
        listTeams.forEach(team -> team.changeStrength());
    }
}
