package com.thirdteam.model.team;

import java.util.Comparator;

public class TeamNameComparator implements Comparator<TeamImpl> {
    public int compare(TeamImpl o1, TeamImpl o2) {
        return o1.getTeamName().compareTo(o2.getTeamName());
    }
}
