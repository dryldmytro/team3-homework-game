package com.thirdteam.model.team;

import com.thirdteam.model.player.PlayerImpl;

public interface Team {

    void printListAllPlayers();

    void printListPlayersMatch();

}
