package com.thirdteam.model.gamer;

import com.thirdteam.model.team.TeamImpl;

import java.util.List;
import java.util.Scanner;

public class Gamer {
    private static final int START_MONEY = 1000;
    private int count;
    private static Scanner scanner = new Scanner(System.in);

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Gamer() {
        this.count = START_MONEY;
    }

    public TeamImpl getTeamForBet(List<TeamImpl> teamList) {
        int userTeam = scanner.nextInt();
        for (TeamImpl team : teamList)
            if ((userTeam) == team.getId())
                return team;
        System.out.println("Bad choose");
        return getTeamForBet(teamList);
    }

    public int getBet() {
        System.out.println("Enter value of bet: ");
        int bet = scanner.nextInt();
        while (bet <= getCount()) {
            count -= bet;
            return bet;
        }
        return getBet();
    }

    public void betWin(boolean isWin, int bet) {
        if (isWin) {
            this.setCount(getCount() + bet * 2);
        } else if (getCount() == 0) {
            System.out.println("You lose.");
        }
    }
}
