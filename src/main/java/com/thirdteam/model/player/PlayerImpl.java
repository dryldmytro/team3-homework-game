package com.thirdteam.model.player;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class PlayerImpl implements Player {
    private int id;
    private static int countReadFilePlayers;
    private String name;

    public int getMAX_STRENGTH() {
        return MAX_STRENGTH;
    }

//    private String name = getRandomName();
    private final static int MAX_STRENGTH = 100;
    private final static int MIN_STRENGTH = 10;
    private int strength = getRandomNumberWithinMinMax(MIN_STRENGTH, MAX_STRENGTH);

    public PlayerImpl(int id) {
        setName(countReadFilePlayers);
        this.id = id;
    }

    public PlayerImpl(String name, int strength) {
        this.id = id;
        this.name = name;
        this.strength = strength;
    }

    public static int getMaxStrength() {
        return MAX_STRENGTH;
    }

    public static int getMinStrength() {
        return MIN_STRENGTH;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(int countReadFilePlayers) {
        try {
            String fileName = "playersNames.txt";
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            ArrayList<String> stringList = new ArrayList<String>();
            String line;
            int counter = 0;
            while ((line = bufferedReader.readLine()) != null) {
                counter++;
                if (counter > (countReadFilePlayers - 1)) stringList.add(line);
                if (counter == countReadFilePlayers) break;
            }
            for (int i = 0; i < stringList.size(); i++) {
                this.name = stringList.get(i);
            }
            this.countReadFilePlayers++;
        } catch (IOException e) {
            this.name = "RandomName";
        }
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength() {
        strength = getRandomNumberWithinMinMax(MIN_STRENGTH, MAX_STRENGTH);
    }

    public static int getRandomNumberWithinMinMax(int min, int max) {
        Random random = new Random();
        return random.nextInt((max - min) + 1) + min;
    }

    @Override
    public String toString() {
        return "PlayerImpl{" +
                "name='" + name + '\'' +
                '}';
    }
}
