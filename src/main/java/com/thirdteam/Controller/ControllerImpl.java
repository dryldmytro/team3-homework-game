package com.thirdteam.Controller;

import com.thirdteam.Main;
import com.thirdteam.model.cup.CupImpl;
import com.thirdteam.model.gamer.Gamer;
import com.thirdteam.model.team.TeamImpl;

import javax.sound.midi.Soundbank;
import java.sql.SQLOutput;
import java.util.Scanner;

public class ControllerImpl implements Controller {
    private CupImpl cup = new CupImpl();
    private Gamer gamer = new Gamer();
    private Scanner input = new Scanner(System.in);

    public void createListTeams() {
        cup.fillListTeams();
    }

    public void printListTeam() {
        cup.printTeamList();
    }

    public void searchTemplateTeamById() {
        System.out.println("List all teams");
        printListTeam();
        System.out.println("enter id the team");
    }

    public void printListPlayersMatch() {
        searchTemplateTeamById();
        int id = input.nextInt();
        TeamImpl team = cup.getTeamById(id);
        team.printListPlayersMatch();
    }

    public void printListPlayersTeam() {
        searchTemplateTeamById();
        int id = input.nextInt();
        TeamImpl team = cup.getTeamById(id);
        team.printListAllPlayers();
    }

    public void makeBet() {
        System.out.println("Choose team for bet: ");
        cup.printTeamList();
        TeamImpl myTeam = gamer.getTeamForBet(cup.getListTeams());
        cup.setMyTeam(myTeam);
    }

    public void play() {
        makeBet();
        increaseChancesToWin();
        int bet = gamer.getBet();
        cup.tournamentGrid();
        cup.getMyTeam().printListPlayersMatch();
        boolean isWin = cup.checkIsWin();
        gamer.betWin(isWin, bet);
        System.out.println(gamer.getCount());
        if (cup.getListTeams().size() == 1) {
            System.out.println("End of tournament");
            Runtime.getRuntime().exit(0);
        }
        if (gamer.getCount() == 0) {
            getUserChoice();
        } else {
            play();
        }
    }

    public void increaseChancesToWin() {
        System.out.println("Would you like to increase your chances of winning this bet?\nIt'll cost you 100");
        System.out.println("P.S You still may lose your money\n1-Yes\n2-No");
        if (input.nextInt() == 1) {
            int cost = 100;
            if(gamer.getCount() > cost){
                for (TeamImpl team : cup.getListTeams()) {
                    if (team.getId() == cup.getMyTeam().getId()) {
                        team.setTotalStrenghtOfTeam(team.getTotalStrengthTeam() / TeamImpl.getQuantityPlayersOnMatch());
                        gamer.setCount(gamer.getCount() - cost);
                        System.out.println(gamer.getCount());
                    }
                }
            }
            else{
                System.out.println("You don't have  money to increase the chance");
            }
        }
    }


    public void getUserChoice() {
        System.out.println("You have lost all your money");
        if (cup.getListTeams().size() >= 1) {
            System.out.println("Want to see how it is gonna end without betting? ");
            System.out.println("1. Yes\n2. No");
            switch (input.nextInt()) {
                case (1):
                    playWithoutBet();
                case (2):
                    Runtime.getRuntime().exit(0);
            }
        }
    }

    public void playWithoutBet() {
        cup.tournamentGrid();
        cup.getMyTeam().printListPlayersMatch();
        playWithoutBet();
    }
}
