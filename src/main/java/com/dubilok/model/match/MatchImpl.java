package com.dubilok.model.match;

import com.dubilok.model.team.TeamImpl;

import java.util.Random;

public class MatchImpl implements Match {
    public String play(TeamImpl firstTeam, TeamImpl secondTeam) {
        Random random = new Random();
        int winner = 1 + random.nextInt(6);
        int looser = random.nextInt(winner);
        double percent = firstTeam.getStrengthPlayersWhichPlay() + secondTeam.getStrengthPlayersWhichPlay();
        percent = firstTeam.getStrengthPlayersWhichPlay() / percent * 100;
        double randomCount = Math.random() * 100;
        if (randomCount <= percent) {
            secondTeam.setAlive(false);
            return firstTeam.getTeamName() + " " + winner + " - "
                    + looser + " " + secondTeam.getTeamName();

        } else {
            firstTeam.setAlive(false);
            return firstTeam.getTeamName() + " " + looser + " - "
                    + winner + " " + secondTeam.getTeamName();
        }
    }
}
