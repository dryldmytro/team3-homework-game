package com.dubilok.model.match;

import com.dubilok.model.team.TeamImpl;

public interface Match {
    String play(TeamImpl firstTeam, TeamImpl secondTeam);
}
