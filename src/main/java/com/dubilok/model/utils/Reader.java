package com.dubilok.model.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;

public class Reader {

    public static void readDataFromFile(BufferedReader bufferedReader
            , ArrayList<String> stringList, int count) throws IOException {
        String line;
        int counter = 0;
        while ((line = bufferedReader.readLine()) != null) {
            counter++;
            if (counter > (count - 1)) {
                stringList.add(line);
            }
            if (counter == count) {
                break;
            }
        }
        bufferedReader.close();
    }

}
