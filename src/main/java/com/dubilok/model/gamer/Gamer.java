package com.dubilok.model.gamer;

import com.dubilok.model.team.TeamImpl;

import java.util.List;
import java.util.Scanner;

public class Gamer implements BetWin {
    private static final int START_MONEY = 1000;
    private int count;
    private static Scanner scanner = new Scanner(System.in);

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Gamer() {
        this.count = START_MONEY;
    }

    public TeamImpl getTeamForBet(List<TeamImpl> teamList) {
        int userTeam = scanner.nextInt();
        return getTeamForBet(teamList, userTeam);
    }

    public TeamImpl getTeamForBet(List<TeamImpl> teamList, int userTeam) {
        while (userTeam < teamList.size()) {
            return teamList.get(userTeam);
        }
        System.out.println("Bad choose");
        return getTeamForBet(teamList);
    }

    public int getBet() {
        System.out.println("Enter value of bet: ");
        int bet = scanner.nextInt();
        return getBet(bet);
    }

    public int getBet(int bet) {
        while (bet <= count) {
            count -= bet;
            return bet;
        }
        return getBet();
    }

    public void betWin(boolean isWin, int bet) {
        if (isWin) {
            this.setCount(count + bet * 2);
        }
    }
}
