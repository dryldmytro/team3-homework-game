package com.dubilok.model.cup;

import com.dubilok.model.match.Match;
import com.dubilok.model.match.MatchImpl;
import com.dubilok.model.player.PlayerImpl;
import com.dubilok.model.team.TeamImpl;
import com.dubilok.model.team.TeamNameComparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CupImpl implements Cup {
    private final int QUANTITY_TEAMS = 16;
    private List<TeamImpl> listTeams = new ArrayList<>();
    private TeamNameComparator nameComparator = new TeamNameComparator();
    private Match match = new MatchImpl();
    private TeamImpl myTeam;

    public CupImpl() {
    }

    public void printTeamList() {
        for (TeamImpl team : listTeams) {
            System.out.println("id - " + team.getId() + "   " + "name - " + team.getTeamName().toUpperCase());
        }
    }

    public List<TeamImpl> getListTeams() {
        return listTeams;
    }

    public TeamImpl getMyTeam() {
        return myTeam;
    }

    public void setMyTeam(TeamImpl myTeam) {
        this.myTeam = myTeam;
    }

    public void printTeamListSortByName() {
        listTeams.sort(nameComparator);
        printTeamList();

    }

    public void fillListTeams() {
        for (int i = 0; i < QUANTITY_TEAMS; i++) {
            TeamImpl team = new TeamImpl(i);
            team.setListAllPlayers(getRandomTeam());
            listTeams.add(team);
        }
    }

    public List<PlayerImpl> getRandomTeam() {
        List<PlayerImpl> listRandomPlayers = new ArrayList<PlayerImpl>();
        for (int i = 0; i < TeamImpl.getQuantityPlayers(); i++) {
            listRandomPlayers.add(new PlayerImpl(i));
        }
        return listRandomPlayers;
    }

    public TeamImpl getTeamById(int id) {
        for (TeamImpl team : listTeams) {
            if (team.getId() == id) {
                return team;
            }
        }
        return null;
    }

    public String tournamentGrid() {
        StringBuilder result = new StringBuilder();
        Collections.shuffle(listTeams);
        for (int i = 0; i < listTeams.size(); i = i + 2) {
            result.append(match.play(listTeams.get(i), listTeams.get(i + 1))).append("\n");
            changeStrength();
        }
        Collections.shuffle(listTeams);
        listTeams.removeIf(team -> !team.isAlive());
        for (int i = 0; i < listTeams.size(); i++) {
            listTeams.get(i).setId(i);
        }
        return result.toString();
    }

    public boolean checkIsWin() {
        return listTeams.contains(myTeam);
    }

    public void changeStrength() {
        listTeams.forEach(TeamImpl::changeStrength);
    }
}
