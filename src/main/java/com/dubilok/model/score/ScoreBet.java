package com.dubilok.model.score;

import java.util.List;

public class ScoreBet {
    public static int attempt = 0;
    public static boolean isOpen;
    private String name;
    private int score;

    public ScoreBet() {
    }

    public ScoreBet(String name, int score) {
        this.name = name;
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public static ScoreBet getTheBestAttempt(List<ScoreBet> list) {
        int theBestAttempt = Integer.MIN_VALUE;
        int attemptIndex = 0;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).score > theBestAttempt) {
                theBestAttempt = list.get(i).score;
                attemptIndex = i;
            }
        }
        return list.get(attemptIndex);
    }
}
