package com.dubilok.model.score;

public class ScoreChampions {
    public static int attempt = 0;
    private String nameAttempt;
    private String nemeTeamWin;

    public ScoreChampions() {
    }

    public ScoreChampions(String nameAttempt, String nemeTeamWin) {
        this.nameAttempt = nameAttempt;
        this.nemeTeamWin = nemeTeamWin;
    }

    public String getNameAttempt() {
        return nameAttempt;
    }

    public void setNameAttempt(String nameAttempt) {
        this.nameAttempt = nameAttempt;
    }

    public String getNemeTeamWin() {
        return nemeTeamWin;
    }

    public void setNemeTeamWin(String nemeTeamWin) {
        this.nemeTeamWin = nemeTeamWin;
    }
}
