package com.dubilok.model.team;

public interface Team {

    void printListAllPlayers();

    void printListPlayersMatch();

}
