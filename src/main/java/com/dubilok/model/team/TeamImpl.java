package com.dubilok.model.team;

import com.dubilok.model.player.PlayerImpl;
import com.dubilok.model.player.PlayerNameComparator;
import com.dubilok.model.player.PlayerStrengthComparator;
import com.dubilok.model.utils.Reader;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TeamImpl implements Team {
    private final static int QUANTITY_PLAYERS = 10;
    private final static int QUANTITY_PLAYERS_ON_MATCH = 5;
    private static int countReadFileTeams;
    private PlayerStrengthComparator comparatorStrength = new PlayerStrengthComparator();
    private PlayerNameComparator comparatorName = new PlayerNameComparator();
    private int id;
    private String teamName;
    private List<PlayerImpl> listAllPlayers = new ArrayList<>();
    private boolean isAlive = true;

    public TeamImpl() {
    }

    public TeamImpl(int id) {
        setTeamName(countReadFileTeams);
        this.id = id;
    }

    public TeamImpl(String teamName, int teamStrength, List<PlayerImpl> players) {
        this.teamName = teamName;
        this.listAllPlayers = players;
    }

    public static int getQuantityPlayers() {
        return QUANTITY_PLAYERS;
    }

    public static int getQuantityPlayersOnMatch() {
        return QUANTITY_PLAYERS_ON_MATCH;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public void setTeamName(int countReadFileTeams) {
        if (countReadFileTeams == 15) {
            TeamImpl.countReadFileTeams = 0;
        }
        try {
            String fileName = "C:\\EPAM\\Learning\\Game\\teamsNames.txt";
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            ArrayList<String> stringList = new ArrayList<String>();
            Reader.readDataFromFile(bufferedReader, stringList, countReadFileTeams);
            for (int i = 0; i < stringList.size(); i++) {
                this.teamName = stringList.get(i);
            }
            TeamImpl.countReadFileTeams++;
        } catch (IOException e) {
            this.teamName = "RandomTeam";
        }
    }

    public List<PlayerImpl> getListAllPlayers() {
        return listAllPlayers;
    }

    public void setListAllPlayers(List<PlayerImpl> listAllPlayers) {
        this.listAllPlayers = listAllPlayers;
    }

    public boolean isAlive() {
        return isAlive;
    }

    public void setAlive(boolean alive) {
        isAlive = alive;
    }

    public int getTotalStrengthTeam() {
        int totalStrength = 0;
        for (PlayerImpl player : listAllPlayers) {
            totalStrength += player.getStrength();
        }
        return totalStrength;
    }

    public int getStrengthPlayersWhichPlay() {
        int totalStrength = 0;
        listAllPlayers.sort(comparatorStrength);
        for (int i = 0; i < QUANTITY_PLAYERS_ON_MATCH; i++) {
            PlayerImpl player = listAllPlayers.get(i);
            totalStrength += player.getStrength();
        }
        return totalStrength;
    }

    public void printListAllPlayersSortBySrtength() {
        listAllPlayers.sort(comparatorName);
        printListAllPlayers();
    }

    public void printListAllPlayersSortByName() {
        listAllPlayers.sort(comparatorName);
        printListAllPlayers();
    }

    public void printListAllPlayers() {
        listAllPlayers.sort(comparatorStrength);
        for (PlayerImpl player : listAllPlayers) {
            System.out.println("id - " + player.getId() + "  " + "name - " + player.getName()
                    + "  " + "strength - " + player.getStrength());
        }
    }

    public void printListPlayersMatch() {
        listAllPlayers.sort(comparatorStrength);
        for (int i = 0; i < QUANTITY_PLAYERS_ON_MATCH; i++) {
            PlayerImpl player = listAllPlayers.get(i);
            System.out.println("id - " + player.getId() + "  " + "name - " + player.getName()
                    + "  " + "strength - " + player.getStrength());
        }
    }

    public PlayerImpl getPlayerFromListAllPlayersById(int id) {
        for (PlayerImpl player : listAllPlayers) {
            if (player.getId() == id) {
                return player;
            }
        }
        return null;
    }

    public void changeStrength() {
        listAllPlayers.forEach(PlayerImpl::setStrength);
    }

    @Override
    public String toString() {
        return "TeamImpl{"
                + "teamName='" + teamName + '\''
                + ", players=" + listAllPlayers
                + '}';
    }
}
