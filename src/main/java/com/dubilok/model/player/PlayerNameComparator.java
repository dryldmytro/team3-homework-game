package com.dubilok.model.player;

import java.util.Comparator;

public class PlayerNameComparator implements Comparator<PlayerImpl> {
    public int compare(PlayerImpl o1, PlayerImpl o2) {
        return o1.getName().compareTo(o2.getName());
    }
}
