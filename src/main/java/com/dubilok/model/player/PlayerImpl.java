package com.dubilok.model.player;

import com.dubilok.model.utils.Reader;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class PlayerImpl {
    private int id;
    private static int countReadFilePlayers;
    private String name;
    private final int MAX_STRENGTH = 100;
    private final int MIN_STRENGTH = 10;
    private int strength = getRandomNumberWithinMinMax(MIN_STRENGTH, MAX_STRENGTH);

    public PlayerImpl() {
    }

    public PlayerImpl(int id) {
        setName(countReadFilePlayers);
        this.id = id;
    }

    public PlayerImpl(String name, int strength) {
        this.id = id;
        this.name = name;
        this.strength = strength;
    }

    public void setName(int countReadFilePlayers) {
        if (countReadFilePlayers == 15) {
            PlayerImpl.countReadFilePlayers = 0;
        }
        try {
            String fileName = "C:\\EPAM\\Learning\\Game\\playersNames.txt";
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            ArrayList<String> stringList = new ArrayList<>();
            Reader.readDataFromFile(bufferedReader, stringList, countReadFilePlayers);
            for (int i = 0; i < stringList.size(); i++) {
                this.name = stringList.get(i);
            }
            PlayerImpl.countReadFilePlayers++;
        } catch (IOException e) {
            this.name = "RandomName";
        }
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength() {
        strength = getRandomNumberWithinMinMax(MIN_STRENGTH, MAX_STRENGTH);
    }

    public static int getRandomNumberWithinMinMax(int min, int max) {
        Random random = new Random();
        return random.nextInt((max - min) + 1) + min;
    }

    public static String getRandomName() {
        int min = 4;
        int max = 10;
        int length = getRandomNumberWithinMinMax(min, max);
        String alphabet = "abcadefghieijjoklmnopqrsuvewuxyiyz";
        StringBuilder name = new StringBuilder();
        int randomNumber = 0;
        for (int i = 0; i < length; i++) {
            randomNumber = getRandomNumberWithinMinMax(0, 33);
            name.append(alphabet, randomNumber, randomNumber + 1);
        }
        return name.toString();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "PlayerImpl{" +
                "name='" + name + '\'' +
                '}';
    }
}
