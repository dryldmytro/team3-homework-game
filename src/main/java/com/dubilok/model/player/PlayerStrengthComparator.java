package com.dubilok.model.player;

import java.util.Comparator;

public class PlayerStrengthComparator implements Comparator<PlayerImpl> {
    public int compare(PlayerImpl o1, PlayerImpl o2) {
        if (o1.getStrength() > o2.getStrength()) {
            return -1;
        } else {
            return 1;
        }
    }
}
