package com.dubilok.service;

import com.dubilok.model.cup.CupImpl;
import com.dubilok.model.gamer.Gamer;
import com.dubilok.model.team.TeamImpl;

import java.util.List;
import java.util.Scanner;

public class ManagerImpl implements Manager {
    private CupImpl cup = new CupImpl();
    private Gamer gamer = new Gamer();
    private Scanner input = new Scanner(System.in);

    public CupImpl getCup() {
        return cup;
    }

    public void setCup(CupImpl cup) {
        this.cup = cup;
    }

    public Gamer getGamer() {
        return gamer;
    }

    public void setGamer(Gamer gamer) {
        this.gamer = gamer;
    }

    public ManagerImpl() {
        createListTeams();
    }

    public void createListTeams() {
        cup.fillListTeams();
    }

    public void printListTeam() {
        cup.printTeamList();
    }

    public List<TeamImpl> getListTeams() {
        return cup.getListTeams();
    }

    public void searchTemplateTeamById() {
        System.out.println("List all teams");
        printListTeam();
        System.out.println("enter id the team");
    }

    public void printListPlayersMatch() {
        searchTemplateTeamById();
        int id = input.nextInt();
        TeamImpl team = cup.getTeamById(id);
        team.printListPlayersMatch();
    }

    public void printListPlayersTeam() {
        searchTemplateTeamById();
        int id = input.nextInt();
        TeamImpl team = cup.getTeamById(id);
        team.printListAllPlayers();
    }

    public void makeBet() {
        System.out.println("Choose team for bet: ");
        cup.printTeamList();
        TeamImpl myTeam = gamer.getTeamForBet(cup.getListTeams());
        cup.setMyTeam(myTeam);
    }

    public void makeBet(int team) {
        TeamImpl myTeam = gamer.getTeamForBet(cup.getListTeams(), team);
        cup.setMyTeam(myTeam);
    }

    public void play() {
        makeBet();
        int bet = gamer.getBet();
        cup.tournamentGrid();
        cup.getMyTeam().printListPlayersMatch();
        boolean isWin = cup.checkIsWin();
        gamer.betWin(isWin, bet);
        System.out.println(gamer.getCount());
        play();
    }

    public String play(int team, int bet) {
        makeBet(team);
        int gamerBet = gamer.getBet(bet);
        String str = cup.tournamentGrid();
        boolean isWin = cup.checkIsWin();
        gamer.betWin(isWin, gamerBet);
        return str;
    }
}
