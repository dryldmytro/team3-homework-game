package com.dubilok.forms;

import com.dubilok.model.team.TeamImpl;

public class TeamForm {

    private int id;
    private String teamName;

    public TeamForm() {
    }

    public TeamForm(int id, String teamName) {
        this.id = id;
        this.teamName = teamName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public static TeamImpl from(TeamForm teamForm) {
        TeamImpl team = new TeamImpl();
        team.setId(teamForm.id);
        team.setTeamName(teamForm.teamName);
        return team;
    }
}
