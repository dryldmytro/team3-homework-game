package com.dubilok.forms;

import com.dubilok.model.gamer.Gamer;

public class GamerForm {
    private int count;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public GamerForm() {
    }

    public GamerForm(int count) {
        this.count = count;
    }

    public static Gamer from(GamerForm gamerForm) {
        Gamer gamer = new Gamer();
        gamer.setCount(gamerForm.getCount());
        return gamer;
    }
}
