package com.dubilok.controller;

import com.dubilok.forms.GamerForm;
import com.dubilok.forms.TeamForm;
import com.dubilok.model.gamer.Gamer;
import com.dubilok.model.score.ScoreBet;
import com.dubilok.model.score.ScoreChampions;
import com.dubilok.model.team.TeamImpl;
import com.dubilok.service.ManagerImpl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

@Controller
public class GameController {

    private ManagerImpl controller = new ManagerImpl();
    private ArrayList<ScoreBet> scoreList = new ArrayList<>();
    private ArrayList<ScoreChampions> listChampions = new ArrayList<>();
    private TeamImpl form;
    private Gamer localGamer;

    @RequestMapping(path = "/users", method = RequestMethod.GET)
    public ModelAndView getAllUsers() {
        ModelAndView modelAndView = new ModelAndView("users");
        List<TeamImpl> listTeams = controller.getListTeams();
        modelAndView.addObject("team", listTeams);
        return modelAndView;
    }

    @RequestMapping(path = "/", method = RequestMethod.GET)
    public ModelAndView main() {
        return new ModelAndView("start");
    }

    @RequestMapping(path = "/cup", method = RequestMethod.GET)
    public ModelAndView makeCup() {
        ModelAndView modelAndView = new ModelAndView("cup");
        List<TeamImpl> listTeams = controller.getListTeams();
        modelAndView.addObject("maxValueTeam", listTeams.size() - 1);
        modelAndView.addObject("maxValueForBet", controller.getGamer().getCount());
        modelAndView.addObject("finish", listTeams.size());
        modelAndView.addObject("team", listTeams);
        if (controller.getListTeams().size() == 1 && !ScoreBet.isOpen) {
            scoreList.add(new ScoreBet("Attempt " + ++ScoreBet.attempt, controller.getGamer().getCount()));
            listChampions.add(new ScoreChampions("Attempt " + ++ScoreChampions.attempt, controller.getListTeams().get(0).getTeamName()));
            ScoreBet.isOpen = true;
        }
        return modelAndView;
    }

    @RequestMapping(path = "/ppp", method = RequestMethod.POST)
    public String addUser(TeamForm teamForm, GamerForm gamerForm) {
        form = TeamForm.from(teamForm);
        localGamer = GamerForm.from(gamerForm);
        return "redirect:cupCommand";
    }

    @RequestMapping(path = "/cupCommand", method = RequestMethod.GET)
    public ModelAndView getInfoAboutRound() {
        ModelAndView modelAndView = new ModelAndView("cupCommand");
        int team = form.getId();
        int bet = localGamer.getCount();
        String play = controller.play(team, bet);
        String[] mas = play.split("\n");
        modelAndView.addObject("str", mas);
        modelAndView.addObject("count", controller.getGamer().getCount());
        return modelAndView;
    }

    @RequestMapping(path = "/generate", method = RequestMethod.GET)
    public String generateNewTeams() {
        controller = new ManagerImpl();
        ScoreBet.isOpen = false;
        return "redirect:cup";
    }

    @RequestMapping(path = "/score", method = RequestMethod.GET)
    public ModelAndView getScore() {
        ModelAndView modelAndView = new ModelAndView("score");
        modelAndView.addObject("scoreList", scoreList);
        if (scoreList.size() != 0) {
            modelAndView.addObject("bestAttempt", ScoreBet.getTheBestAttempt(scoreList));
        }
        return modelAndView;
    }

    @RequestMapping(path = "/champions", method = RequestMethod.GET)
    public ModelAndView getChampions() {
        ModelAndView modelAndView = new ModelAndView("champions");
        if (controller.getListTeams().size() != 0) {
            modelAndView.addObject("list", listChampions);
        }
        return modelAndView;
    }

}
